from django.db import models
from django.urls import reverse


class User_details(models.Model):
 
    user_name = models.CharField(max_length=100)
    mobile    = models.CharField(max_length=10)
    location  = models.CharField(max_length=100)
    address   = models.TextField()
    
    def get_absolute_url(self):
        return reverse('form_app:index')
        
    class Meta:
        db_table = 'user_details'