from django.shortcuts import render

from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
 

from .models import User_details
 
class IndexView(generic.ListView):
    context_object_name = 'User_details_list'
    template_name = 'form_app/index.html'
 
    def get_queryset(self):
        return User_details.objects.all()
 

class User_detailsEntry(CreateView):
    model = User_details
    fields = ['user_name', 'mobile', 'location', 'address']
 

class User_detailsUpdate(UpdateView):
    model = User_details
    fields = ['user_name', 'mobile', 'location', 'address']
    
    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        # Next, try looking up by primary key.
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        slug = self.kwargs.get(self.slug_url_kwarg, None)
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        # Next, try looking up by slug.
        elif slug is not None:
            slug_field = self.get_slug_field()
            queryset = queryset.filter(**{slug_field: slug})
        # If none of those are defined, it's an error.
        else:
            raise AttributeError("Generic detail view %s must be called with "
                                 "either an object pk or a slug."
                                 % self.__class__.__name__)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except ObjectDoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj
 

class User_detailsDelete(DeleteView):
    model = User_details
    success_url = reverse_lazy('form_app:index')