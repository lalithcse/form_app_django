from django.conf.urls import url
 
from form_app import views
 
app_name = 'form_app'
 
urlpatterns = [
 
    # /modelforms/
    url(r'^$', views.IndexView.as_view(), name='index'),
 
    # modelforms/product/entry
    url(r'^user_details/entry/$',views.User_detailsEntry.as_view(),name='user-entry'),
 
    # modelforms/product/2
    url(r'^user_details/(?P<pk>[0-9]+)/$', views.User_detailsUpdate.as_view(), name='user-update'),
 
    # modelforms/product/(?P<pk>[0-9]+)/delete
    url(r'^users/(?P<pk>[0-9]+)/delete$', views.User_detailsDelete.as_view(), name='user-delete'),
 
]